<!--
This template is used by the EMO to track the annual review of an Interest Group.

You can delete the comments (or not).

NOTES for the user:

- The name of this ticket should follow the format: YYYY.MM IG_name Interest Group
- The IG creation ticket should be linked in this issue.
- Replace the field IG_name in this template with the name of the IG in object.

-->

The EMO is using this issue to track the progress of the annual review for the _IG_name_ Interest Group. Help regarding the process can be found in the [Eclipse Foundation Industry Collaboration process](https://www.eclipse.org/org/collaborations/interest-groups/process.php).

We are using scoped labels to identify where an Interest Group proposal is at any time during the onboarding process. If you have doubts, just ask.

We'll use the due date of this issue to provide an estimation of the next date the EMO team will check in with the interest group team. Sometimes, e.g. during the project creation time, the due date will coincide with the Interest Group Creation date.

## Interest Group

- Interest Group name: [**IG_name**](url)

## Success criteria for the annual program review

- [ ] Operating in an open and transparent manner
- [ ] Activity on the Interest Group mailing list
- [ ] At least three participating members with Member Committer and Contributor Agreements in place
- [ ] Produce an annual activity report
- [ ] Executive Director approval to continue in the Operational Phase or move to Termination

## Review Result
 - [ ] Pass of annual program review 
 - [ ] Fail of annual program review, move to termination 
 
/cc @scorbett @mdelgado624
