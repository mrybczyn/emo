<!--
This template is used by the EMO to track the creation of an Interest Group.

If you want to create a new Eclipse interest group start here:  https://www.eclipse.org/collaborations/#start-collaboration

Set the title of this issue to the name of the Interest Group.

You can delete the comments (or not).
-->

The EMO is using this issue to track the progress of Interest Group creation. Help regarding the process can be found in the [Eclipse Foundation Industry Collaboration process](https://www.eclipse.org/org/collaborations/interest-groups/process.php).

<!-- The following image shows the workflow for [Starting an Interest Group](image_link) at the Eclipse Foundation.-->

We are using scoped labels to identify where an Interest Group proposal is at any time during the onboarding process. These labels are mapped to the stages shown in the previous image. If you have doubts, just ask.

We'll use the due date of this issue to provide an estimation of the next date the EMO team will check in with the interest group team. Sometimes, e.g. during the project creation time, the due date will coincide with the Interest Group Creation date.


## Interest Group

<!-- Proposed Interest Group name and proposal link -->

- Interest Group name: {name}
- Interest Group proposal: {url}


## Basic Information

- Scope: {scope}
- License: Creative Commons Attribution 4.0 International (CC BY 4.0) 

> Interest Groups may, at their discretion, produce artifacts such as documents, whitepapers, architectures, blueprints, diagrams, presentations, and the like; however, Interest Groups must not develop software, software documentation or specifications.

- Participating members

## Pre-Proposal Phase Checklist

_To be completed by the EMO_

- [ ] Member organization(s) declares their interest in, and rationale for establishing the Interest Group
- [ ] Scope well defined - ED approval needed
- [ ] Name chosen
- [ ] Interest Group lead(s) identified (must have at least one)

## Pre-Proposal Phase ends

_To be completed by @scorbett_

- [ ] Proposed Interest Group announced to Membership At Large

## Proposal Phase Checklist

- [ ] Interest group name trademark approved (Interest Groups must choose a name, the Eclipse Foundation retains ownership. [Trademark transfer agreement available here](https://www.eclipse.org/legal/Trademark_Transfer_Agreement.pdf) in case its needed)
- [ ] At least three members participating
- [ ] Participant(s) identified for each member
- [ ] Participants have [Eclipse Foundation Accounts](https://accounts.eclipse.org/user/register?destination=user)
- [ ] All members have MCCA on file
- [ ] 2 weeks of review completed
 
## Interest Group resources to be created

 _**Interest Group Lead** involvement required_. 
Interest Groups at the Eclipse Foundation may make use of certain Eclipse Foundation services. 

- [ ] The interest group lead has confirmed the project shortname: **AAAAAAAA**
- [ ] Interest Group mailing list:  https:accounts.eclipse.org/mailing-list/**name**-ig@eclipse.org
- [ ] GitLab repository name:
    - **Name IG**  
    - URL: https://gitlab.eclipse.org/eclipse-ig/name-ig

## Post-creation Checklist
_To be completed by the EMO_
- [ ] EF Database entry created
- [ ] Update PMI IG status (set to active)  
- [ ] Project resources provisioning (e.g. website, repositories, etc.)
- [ ] Annual review record created


## What's next?
Upon completion of the proposal phase, the Interest Group moves into the Operational Phase. During the Operational Phase, the Members involved in the Interest Group will work collaboratively on the various initiatives and materials that they collectively identify. This collaborative work must conform to this [Interest Group Process](https://www.eclipse.org/org/collaborations/interest-groups/process.php), but otherwise Members are generally left to decide the specifics.

Interest Groups must produce an annual activity report and pass an annual program review. See you then!

<!-- Quick actions will configure the state of the issue. Leave these. -->
/label ~"Interest Groups" ~"Trademark Approval Request"
/assign @scorbett @mdelgado624
/confidential
