<!--
There's help in the Eclipse Foundation Project Handbook https://www.eclipse.org/projects/handbook/#release-review

Parts of this issue can initially be left blank to be filled in during the review.

Set the title of this issue to the project's id, e.g., "technology.dash"

You can delete the comments (or not).
-->
## Project

[{Project Name}]({pmi link})

The EMO is using this issue to track this progress review. Help regarding the process can be found in the [Eclipse Foundation Project Handbook](https://www.eclipse.org/projects/handbook/#release-review).

## What now?

If you have doubts about the Eclipse Development process, we suggest you read this paragraph of the Eclipse Handbook on Progress and Release Reviews: https://www.eclipse.org/projects/handbook/#release-review
<!--
This being a specification project, you'll also need to follow some extra guidelines, usually you'll need to run a Specification Committee Ballot before the release review date. Please make sure to check your Working group guidelines regarding Specification releases.
-->

The following image shows the workflow for a Progress/Release review at the Eclipse Foundation.

![Review Process](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/raw/main/Template%20Images/EDP_releases.png)
<!--
Only for specification projects
![Review Process for Specification Projects](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/raw/main/Template%20Images/EDP_release_specification.png)
-->

## Requisites
- [ ] [3rd party dependencies provided (i.e. notice file, SBOM, etc. )]({IPzilla link})
- [ ] [PMC approval requested]({mailing list message link})
<!--
Only for specification projects
- [ ] [WG ballot initiated]({mailing list message link})
-->

## Intellectual Property Management

_To be completed by the EMO._

- [ ] All project code has [copyright and license headers](https://www.eclipse.org/projects/handbook/#ip-copyright-headers) correctly applied. ** EMO will scan the code at their discretion **
- [ ] All distributed third-party content has been vetted by the IP Due Diligence process (i.e., IP Log has been approved)

## Open Source Rules of Engagement

_To be completed by the PMC in the case of Progress Reviews._

- [ ] PMC approval [on the mailing list]({link to the message})

General:

- [ ] Project is operating within the mission and scope defined in its top-level project’s charter
- [ ] Project is operating within the bounds of its own scope
- [ ] Project is operating in an open and transparent manner
- [ ] Overall the project is operating according to the Eclipse Development Process. 

Things to check:
  - Communication channels advertised
  - Advertised communication channels used
  - Committers are responding to questions
  - Committers are responding to issues
  - Committers are responding to pull/merge/review requests

## Branding and Trademarks

_To be completed by the EMO and PMC._

The following applies when the project has a custom website.

To the best of our knowledge:
- [ ] Project content correctly uses Eclipse Foundation trademarks
- [ ] Project content (code and documentation) does not violate trademarks owned by other organizations

Things to check:
- Project website uses the [project's formal name](https://www.eclipse.org/projects/handbook/#trademarks-website-name) in first and all prominent references 
- Project website includes a [trademark attribution statement]((https://www.eclipse.org/projects/handbook/#trademarks-website))
- Project website footers contain all [necessary elements](https://www.eclipse.org/projects/handbook/#trademarks-website-footer)

<!--## Security

_To be completed by the EMO._

The following are EF recommendations to improve the security of your Open Source Project. 
- [ ] Implement 2-factor authentication to login into your GitLab/GitHub accounts.
- [ ] Enabling Security health metrics for Open Source ([OSSF Scorecard](https://github.com/ossf/scorecard#readme))

You can also request a personalized security assessment from the EF Security Team of your project repositories and related resources.
- [ ] Request Security Team assessment
-->

## Legal Documentation

Required files:

- [ ] License files in all repository roots
- [ ] README
- [ ] CONTRIBUTING (or equivalent)

Recommended files: 

- [ ] NOTICES or equivalent (you can use the [Legal Documentation generator](https://www.eclipse.org/projects/tools/documentation.php) available under committer tools) 
- [ ] CODE_OF_CONDUCT
- [ ] SECURITY 

See examples for [Security file](https://github.com/eclipse-ee4j/.github/blob/main/SECURITY.md) and [Code of Conduct](https://github.com/eclipse/.github/blob/master/CODE_OF_CONDUCT.md).

Required elements:

- [ ] ECA is referenced/described

Recommended elements: 

- [ ] Build instructions provided
- [ ] [Security policy](https://www.eclipse.org/security/policy.php) is described

## Metadata (PMI)

_To be completed by the EMO and PMC._

- [ ] The formal name, e.g. "Eclipse Foo™", is used in the project title
- [ ] The formal name including appropriate marks (e.g, "™") is used in the first mention in the text of the project description, and scope
- [ ] The project description starts with a single paragraph that can serve as an executive summary
- [ ] Source code repository references are up-to-date
- [ ] Download links and information are up-to-date (see [EF handbook for more information on how-to do this](https://www.eclipse.org/projects/handbook/#resources-downloads))
- [ ] Communication channels listed in the PMI (i.e. public mailing list, forums, etc.)

<!-- Quick actions will configure the state of the issue. Leave these. -->
/label ~"Review Request" ~"IP review" ~"PMC approval"
/assign @wbeaton @mdelgado624
