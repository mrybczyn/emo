<!--
This template is used by the EMO to track the creation of a project.

If you want to create a new Eclipse open source project start here:  https://www.eclipse.org/projects/handbook/#starting

Parts of this issue can initially be left blank.

Set the title of this issue to the name of the proposal

You can delete the comments (or not).
-->

The EMO is using this issue to track the progress of project creation. Help regarding the process can be found in the [Eclipse Foundation Project Handbook](https://www.eclipse.org/projects/handbook/#starting).

The following image shows the workflow for [Starting a Project](https://www.eclipse.org/projects/handbook/#starting) at the Eclipse Foundation.

<!--For SPEC projects
![Onbarding Process for Specification Projects](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/raw/main/Template%20Images/Starting_a_project_EF_spec.png)
-->
![Onbarding Process](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/raw/main/Template%20Images/Starting_a_project_EF.png)

We are using scoped labels to identify where a project proposal is at any time during the onboarding process. These labels are mapped to the stages shown in the previous image and will define the "state" of this ticket. If you have doubts, just ask.

We'll use the due date of this issue to provide an estimation of the next date the EMO team will checkin with the project team. Sometimes, e.g. during the project creation time, the due date will coincide with the Project Creation date.

## Project

<!-- Proposed project name and proposal link from the PMI -->

Project proposal: [{project_name}](https://projects.eclipse.org/proposals/{proposal})

Top level project: 

## Basic Information

- License: {license}
<!-- Specification projects only
- Patent License {Patent license}
-->

## Pre-creation Checklist

_To be completed by the EMO_

- [ ] GitLab ticket state set to Community Review
- [ ] Project scope is well-defined
- [ ] Top-level Project selected
- [ ] PMC accepts the project (PMC has replied with +1)
- [ ] License(s) approved
- [ ] Project name -nor similar names- has NOT been previously used by the team
- [ ] Project name trademark approved. [Trademark transfer agreement available here](https://www.eclipse.org/legal/Trademark_Transfer_Agreement.pdf) in case you need it.
- [ ] Project lead(s) identified
- [ ] Committers identified
- [ ] Committers have Eclipse Foundation Accounts
- [ ] Eclipse Architecture Council mentor identified
- [ ] Two weeks of community review completed

<!-- Specification projects only
Being this a specification project, a Spec Committee ballot to aprove the project creation must run for at least one week
- [ ] Specification Committee ballot initiated 
- [ ] Specification Committee ballot concluded successfully
-->

## Project resources to be created 
_Project Lead involvement needed_
Open source projects at the Eclipse Foundation are required to make use of certain Eclipse Foundation services. See the [Handbook](https://www.eclipse.org/projects/handbook/#project-resources-and-services) for more information on this subject.

- [ ] The project lead has confirmed the project shortname: AAAAAAAA 
- [ ] The project lead has requested a project website repository in the comments
- [ ] The project lead has confirmed the following project resources are correct in the comment's section


<!--
GitLab
- Project repository group: https://gitlab.eclipse.org/eclipse/{project_name}
- Project repository: https://gitlab.eclipse.org/eclipse/{project_name}/{project_name}  
GitHub
- Project repository: https://github.com/eclipse/{project_name}
- Downloads: http://download.eclipse.org/{project_name}
- Archives: http://archive.eclipse.org/{project_name}
-->
- Project mailing list: https://accounts.eclipse.org/mailing-list/{project_name}-dev


## Post-creation Checklist

_To be completed by the EMO_

EMO internal tasks:
- [ ] Project DB entry created 
- [ ] Project lead(s) designated (DB)
- [ ] Architecture Council mentor designated (DB)

Milestones:
- [ ] Project created
- [ ] Project provisioned (including at least one committer)
- [ ] Committer orientation completed (upon request)

## After creation
Once the project is created the [provisioning process](https://www.eclipse.org/projects/handbook/#starting-provisioning) starts with an email message being sent to each of the committers listed on the project proposal with instructions on how to engage in the [committer paperwork](https://www.eclipse.org/projects/handbook/#paperwork) process. 

Committers should check their emails and complete the paperwork at their earliest convenience. **No project resources are created until at least one committer is fully provisioned.**

_To be completed by the EMO_
- [ ] GitLab ticket state set to Provisioning

## After provisioning

Following provisioning, the project will be ready for initial contribution review. An initial contribution is basically the first set of project code contributions to the new Eclipse project. An initial contribution typically takes the form of an entire existing source code repository or some snapshot of an existing code base.

At this point, the project team might proceed to push their code into the requested repositories or coordinate with Webmaster and EMO to transfer existing ones. Once you've completed this process please notify EMO using this ticket.

_To be completed by the EMO_
- [ ] Provide project PMI link to start the IP check <br>
    Project: [Eclipse project_name](https://projects.eclipse.org/projects/tlp.project_name)
- [ ] GitLab ticket state set to Initial Contribution

## IP-check
_To be completed by the EMO_

Once the project has provided their initial contribution:
- [ ] IP check completed
- [ ] GitLab ticket state set to Fully Operational


<!-- Quick actions will configure the state of the issue. Leave these. -->
/label ~"Project Proposal" ~"Trademark Approval Request" ~"Mentor" ~"License Approval Request" ~"EDP::CommunityReview"
<!-- Specification projects only
/label~"ballot
-->
/assign @wbeaton @mdelgado624
